#!/bin/sh
# repack.sh - removes non-free bits from the content of the upstream
# source tarball to provide a DFSG-compliant tarball for the Debian package.
#
# In this case the files removed are:
# - examples/misc/fbo_test.rb
# - examples/NeHe

set -e

DEB_UPSTREAM_VERSION=$(dpkg-parsechangelog | sed -rne 's/^Version: ([^+]+).*/\1/p')

uscan --noconf --verbose --force-download --rename --download-current-version  --destdir=.

rm -rf opengl-${DEB_UPSTREAM_VERSION}

# unpack upstream tarball
tar -xf ruby-opengl_${DEB_UPSTREAM_VERSION}.orig.tar.gz
rm ruby-opengl_${DEB_UPSTREAM_VERSION}.orig.tar.gz

# remove example not allowing distribution if modified
rm ruby-opengl-$DEB_UPSTREAM_VERSION/examples/misc/fbo_test.rb

# remove NeHe examples, without clear license
rm -rf ruby-opengl-$DEB_UPSTREAM_VERSION/examples/NeHe

# repack the tarball
mv ruby-opengl-$DEB_UPSTREAM_VERSION ruby-opengl-$DEB_UPSTREAM_VERSION.orig
GZIP=--best tar --create --gzip --owner root --group root --mode a+rX \
	--file ../ruby-opengl_$DEB_UPSTREAM_VERSION+dfsg2.orig.tar.gz ruby-opengl-$DEB_UPSTREAM_VERSION.orig
rm -r ruby-opengl-$DEB_UPSTREAM_VERSION.orig

