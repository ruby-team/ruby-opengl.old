#-*-ruby-*-
#
# Copyright (C) 2006 John M. Gabriele <jmg3000@gmail.com>
#
# This program is distributed under the terms of the MIT license.
# See the included MIT-LICENSE file for the terms of this license.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

begin
    require 'rubygems'
rescue LoadError
    nil
end

require 'rake'
require 'rake/clean'
require 'rake/gempackagetask'
require 'rake/testtask'

require 'mkrf/rakehelper'

# Generate html docs from the markdown source and upload to the site.
# All doc files that are destined for the website have filenames that
# end in .txt.

WEBSITE_MKDN = FileList['./doc/*.txt'] << 'README.txt'
NICE_HTML_DOCS = WEBSITE_MKDN.ext('html')

# defines columns in the HTML extension list
GLEXT_VERSIONS = ["svn","0.60","0.50"]

CLEAN.include("ext/gl*/Rakefile", "ext/*/mkrf.log", "ext/*/*.so", 
              "ext/**/*.bundle", "lib/*.so", "lib/*.bundle", "ext/*/*.o{,bj}", 
              "ext/*/*.lib", "ext/*/*.exp", "ext/*/*.pdb",
              "pkg")
CLOBBER.include("*.plain", "doc/*.plain", "doc/*.snip", "*.html",
                "doc/*.html", "website/*.html", "website/images/*")
# Make sure these files aren't deleted in a clobber op
CLOBBER.exclude("website/images/tab_bottom.gif")
CLOBBER.exclude("website/images/*.jpg")

setup_extension('gl', 'gl')
setup_extension('glu', 'glu')
setup_extension('glut', 'glut')

case RUBY_PLATFORM
when /(:?mswin|mingw)/
	desc 'Does a full win32 compile'
	task :default do
		exts = ["gl","glu","glut"]
		exts.each do |ext|
			Dir.chdir("ext\\#{ext}") do
				sh "ruby mkrf_conf.rb"
				sh "call rake --nosearch"
				sh "copy #{ext}.so ..\\..\\lib"
			end
		end
	end
else
	desc 'Does a full compile'
	task :default => [:gl, :glu, :glut, :fixpermissions]
end

task :fixpermissions do
	# fix wrong lib permissions (mkrf bug ?)
	Dir["lib/*.so","lib/*.bundle"].each do |fname|
		File.chmod(0755,fname)
	end
end

task :extension => :default

desc 'Show contents of some variables related to website doc generation.'
task :explain_website do
    puts "WEBSITE_MKDN:"
    WEBSITE_MKDN.each do |doc|
        puts "\t#{doc}"
    end
    puts "NICE_HTML_DOCS:"
    NICE_HTML_DOCS.each do |doc|
        puts "\t#{doc}"
    end
end

desc 'Generate supported extension list.'
task :gen_glext_list do
	sh "./utils/extlistgen.rb doc/extensions.txt.in doc/extensions.txt " + GLEXT_VERSIONS.join(" ")
end

desc 'Generate website html.'
task :gen_website => [:gen_glext_list] + NICE_HTML_DOCS do
    # Now that the website docs have been generated, copy them to ./website.
    puts
    sh "cp README.html website/index.html"
    sh "cp doc/*.html website"
end

# You'll see some intermediate .plain files get generated. These are html,
# but don't yet have their code snippets syntax highlighted.
rule '.html' => '.plain' do |t|
    puts "Turning #{t.source} into #{t.name} ..."
    sh "./utils/post-mkdn2html.rb #{t.source} #{t.name}"
end

# Process the markdown docs into plain html.
rule '.plain' => '.txt' do |t|
    puts
    puts "Turning #{t.source} into #{t.name} ..."
    sh "./utils/mkdn2html.rb #{t.source} #{t.name}"
end

desc 'Upload the newly-built site to RubyForge.'
task :upload_website => [:gen_website] do
    sh "scp website/*.html hoanga@rubyforge.org:/var/www/gforge-projects/ruby-opengl"
    sh "scp website/images/* hoanga@rubyforge.org:/var/www/gforge-projects/ruby-opengl/images/"
end

desc 'Upload entire site, including stylesheet and the images directory.'
task :upload_entire_website => [:gen_website] do
    sh "scp website/*.html hoanga@rubyforge.org:/var/www/gforge-projects/ruby-opengl"
    sh "scp website/*.css hoanga@rubyforge.org:/var/www/gforge-projects/ruby-opengl"
    sh "scp -r website/images hoanga@rubyforge.org:/var/www/gforge-projects/ruby-opengl"
end

desc 'Runs unit tests.'
Rake::TestTask.new do |t|
    t.libs << "test"
    t.test_files = FileList['test/tc_*.rb']
    t.verbose = true
end

desc 'Runs unit tests.'
task :test_all => [:test]

# Define the files that will go into the gem
gem_files = FileList["{lib,ext,doc,examples,test}/**/*"]
gem_files = gem_files.exclude("**/*.so", "**/*.o{,bj}", "ext/**/*.log", "ext/gl*/Rakefile")

spec = Gem::Specification.new do |s|
    s.name              = "ruby-opengl"
    s.version           = "0.60.1"
    s.authors           = [ "Alain Hoang", "Jan Dvorak", "Minh Thu Vo", "James Adam" ]
    s.homepage          = "http://ruby-opengl.rubyforge.org"
    s.email             = "ruby-opengl-devel@rubyforge.org"
    s.rubyforge_project = 'ruby-opengl'
    s.platform          = Gem::Platform::RUBY
    s.summary           = "OpenGL Interface for Ruby"
    s.files             = gem_files
    s.extensions        << 'Rakefile'
    s.require_path      = "lib"
    s.autorequire       = "gl"
    s.has_rdoc          = false
    s.add_dependency("mkrf", ">=0.2.0")
    s.add_dependency("rake")
end

# Create a task for creating a ruby gem
Rake::GemPackageTask.new(spec) do |pkg|
    pkg.gem_spec = spec
    pkg.need_tar = true
end
